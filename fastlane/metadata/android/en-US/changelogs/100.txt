2.5.0-rc1

Release notes will be published with the stable release.
In the mean time check the milestone for pull requests and issues that are merged/closed.

https://gitea.com/gitnex/GitNex/milestone/171?q=&type=all&sort=&state=closed&labels=&assignee=0
