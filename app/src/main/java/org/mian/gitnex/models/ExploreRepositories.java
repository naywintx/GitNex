package org.mian.gitnex.models;

import java.util.ArrayList;

/**
 * Author M M Arif
 */

public class ExploreRepositories {

    private ArrayList<UserRepositories> data;
    private Boolean ok;

    public ArrayList<UserRepositories> getSearchedData() {
        return data;
    }

    public Boolean getOk() {
        return ok;
    }

}
